/*############################################################################
## Copyright (c) logi.cals. All rights reserved.
## ---------------------------------------------------------------------------
## TASK      :: Basic Sample
##
## ---------------------------------------------------------------------------
## AUTHOR    :: Mario Semo
## CREATED   :: 12-12-30
## ---------------------------------------------------------------------------
## NOTES     :: none
## ---------------------------------------------------------------------------
############################################################################*/

//*****************************************************************************
// Imports
//*****************************************************************************
import org.apache.log4j.Logger;
import org.apache.log4j.BasicConfigurator;

//****************************************************************************
// Classes
//****************************************************************************

public class LogSample0
{
  private static final Logger smLogger = Logger.getLogger(Hello.class);

  public static void main(String argv[])
  {
	BasicConfigurator.configure();
	smLogger.debug("Hello world.");
   // da braucht man kein "isDebug oder so.... .debug traced nur, wenn debug allowed ist)
	smLogger.info("What a beatiful day.");
  }
}
