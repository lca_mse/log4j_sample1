/*############################################################################
## Copyright (c) logi.cals. All rights reserved.
## ---------------------------------------------------------------------------
## TASK      :: Sample for log4J
##
## ---------------------------------------------------------------------------
## AUTHOR    :: Mario Semo
## CREATED   :: 12-12-30
## ---------------------------------------------------------------------------
## NOTES     :: none
## ---------------------------------------------------------------------------
############################################################################*/

//*****************************************************************************
// Imports
//*****************************************************************************
import java.util.*;
import org.apache.log4j.*;

//****************************************************************************
// Classes
//****************************************************************************
public class LogSample1
{
   private static final Logger smLogger = Logger.getLogger(LogSample1.class.getName());

   public static void main(String[] pArgList)
   {
      BasicConfigurator.configure();

      dumpArgs(pArgList);
      measureLogTime1();
      measureLogTime2();
      measureLogTime3();
      measureLogTime4();
   }

   public static void dumpArgs(String[] pArgList)
   {
      for (int i=0;i<pArgList.length;i++)
      {

         System.out.println(pArgList[i]);
         //if(smLogger.isDebugEnabled())
         //{
           smLogger.debug("DEB>" + pArgList[i]);
         //}
      }
   }

   public static void measureLogTime1()
   {
    smLogger.info("InfoMessage");
    smLogger.debug("DebugMessage");
    Logger.getRootLogger().setLevel(Level.INFO);
    smLogger.info("InfoMessage");
    smLogger.debug("DebugMessage");
    long lStartTime1 = System.currentTimeMillis();
    long lStartTime2 = System.nanoTime();
    for (long i=0;i<100000000;i++)
    {
       smLogger.debug("DebugMessage");
    }
    long lEstimatedTime1 = System.currentTimeMillis() - lStartTime1;
    long lEstimatedTime2 = System.nanoTime() - lStartTime2;

    System.out.println("Execution time was "+lEstimatedTime1+" ms, "+lEstimatedTime2+" ns");
    // Execution time was 1406 ms.
   }

   public static void measureLogTime2()
   {
    Logger.getRootLogger().setLevel(Level.INFO);
    long lStartTime1 = System.currentTimeMillis();
    long lStartTime2 = System.nanoTime();
    for (long i=0;i<100000000;i++)
    {
       if(smLogger.isDebugEnabled())
       {
          smLogger.debug("DebugMessage");
       }
    }
    long lEstimatedTime1 = System.currentTimeMillis() - lStartTime1;
    long lEstimatedTime2 = System.nanoTime() - lStartTime2;

    System.out.println("Execution time was "+lEstimatedTime1+" ms, "+lEstimatedTime2+" ns");
    // Execution time was 1079 ms.
   }
   public static void measureLogTime3()
   {
    Logger.getRootLogger().setLevel(Level.INFO);
    long lStartTime1 = System.currentTimeMillis();
    long lStartTime2 = System.nanoTime();
    boolean lIsDebugEnabled=smLogger.isDebugEnabled();
    for (long i=0;i<100000000;i++)
    {
       if(lIsDebugEnabled)
       {
          smLogger.debug("DebugMessage");
       }
    }
    long lEstimatedTime1 = System.currentTimeMillis() - lStartTime1;
    long lEstimatedTime2 = System.nanoTime() - lStartTime2;

    System.out.println("Execution time was "+lEstimatedTime1+" ms, "+lEstimatedTime2+" ns");
    // Execution time was 1079 ms.
   }
   public static void measureLogTime4()
   {
    Logger.getRootLogger().setLevel(Level.INFO);
    long lStartTime1 = System.currentTimeMillis();
    long lStartTime2 = System.nanoTime();
    boolean lIsDebugEnabled=smLogger.isDebugEnabled();
    if(lIsDebugEnabled)
    {
       for (long i=0;i<100000000;i++)
       {
        smLogger.debug("DebugMessage");
       }
    }
    long lEstimatedTime1 = System.currentTimeMillis() - lStartTime1;
    long lEstimatedTime2 = System.nanoTime() - lStartTime2;

    System.out.println("Execution time was "+lEstimatedTime1+" ms, "+lEstimatedTime2+" ns");
    // Execution time was 1079 ms.
   }
}
