@REM #########################################################################
@REM ## Copyright (c) logi.cals. All rights reserved.
@REM ## ----------------------------------------------------------------------
@REM ## TASK      :: show different variant of logging
@REM ##
@REM ## ----------------------------------------------------------------------
@REM ## AUTHOR    :: Mario Semo
@REM ## CREATED   :: 13-01-08
@REM ## ----------------------------------------------------------------------
@REM ## NOTES     :: none
@REM ## ======================================================================
@REM ## HISTORY   :: $Author: $$Revision: $
@REM ##              $Date: $
@REM ## ----------------------------------------------------------------------
@REM ## $Log: $
@REM #######################################################################*/
@echo off
setlocal

javac LogSample1.java

echo ***
echo *** run without any log4j.xml
echo ***
java LogSample1

echo ***
echo *** run with log4j.v1 as .xml
echo ***
copy log4j.v1 log4j.xml  >nul 2>&1
java LogSample1
del log4j.xml   >nul 2>&1

echo ***
echo *** run with log4j.v2 as .xml (log 2 tmp\example.log
echo ***
copy log4j.v2 log4j.xml >nul
rd /Q /S tmp >nul 2>&1
md tmp  >nul 2>&1
java LogSample1
del log4j.xml   >nul 2>&1
echo .
dir tmp

endlocal
