############################################################################
## Copyright (c) logi.cals. All rights reserved.
## ---------------------------------------------------------------------------
## TASK      :: First Sample for log4j
##
## ---------------------------------------------------------------------------
## AUTHOR    :: Mario Semo
## CREATED   :: 12-12-30
## ---------------------------------------------------------------------------
## NOTES     :: none
## ---------------------------------------------------------------------------
############################################################################

(Das ist die zusammengefasste und geteste Variante aus dem INSTALL File des log4j packages)

This is a pure commandline sample.
NO MAVEN Support
NO Eclipse support.




1) Download log4j

http://logging.apache.org/log4j/1.2/download.html   ->   http://tweedo.com/mirror/apache/logging/log4j/1.2.17/log4j-1.2.17.zip

-> log4j-1.2.17.zip

2) Man entpackt das Package irgendwo. In dem Package ist der komplette SOURCE, Doku, UnitTest,.... und EIN .jar File.

3) Man erweitert den Classpath um DIESES .jar File.

Beispiel: set CLASSPATH=.;D:\LCo3\MS_Java_Samples\log4j-1.2.17.jar

(Achtung: NICHT vergessen auch "." im Classpath aufnehmen, sonst kann man keine Java Apps im CurrDir mehr ausf�hren)

Man �bersetzt folgendes TestProgramm (ACHTUNG auf Gro� und Kleinschreibung, Java ist case-sensitive)
LogSample0.java
Code einblenden

 Und dann f�hrt man es aus:
Compile und ausf�hrung
D:\LCo3\MS_Java_Samples\log4j>javac LogSample0.java

D:\LCo3\MS_Java_Samples\log4j>java LogSample0

0 [main] DEBUG LogSample0  - Hello world.

0 [main] INFO LogSample0  - What a beatiful day.



Will man nun auf ganz einfache Art von der Commandline aus, ohne neu Compilation des JavaCodes, die Logger Configuration �ndern, speichern man im aktuellen Verzeichnis (dort, wo das .class File liegt und aufgerufen wird) folgendes xml File ab:


log4x.xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE log4j:configuration SYSTEM "log4j.dtd">

<log4j:configuration xmlns:log4j="http://jakarta.apache.org/log4j/">
  <root>
    <priority value="INFO"/>
  </root>
</log4j:configuration>

ruft man nun, ohne irgendwas sonst zu �ndern, LogSample0 auf, kommt nur mehr die INFO , aber keine Debug Message mehr:


cmd.exe
D:\LCo3\MS_Java_Samples\log4j>java LogSample0

0 [main] INFO LogSample0  - What a beatiful day.


�ndert man im XML die priority z.B. auf ERROR : <priority value="ERROR"/> und ruft nun wieder "java LogSample0" auf, kommt gar kein Output mehr.
